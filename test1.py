from __future__ import division

import cv2
import glob
import matplotlib.pyplot as plt
import numpy as np
import os
from collections import OrderedDict

import colour
from colour.plotting import *

from colour_checker_detection import (
    EXAMPLES_RESOURCES_DIRECTORY,
    colour_checkers_coordinates_segmentation,
    detect_colour_checkers_segmentation)
from colour_checker_detection.detection.segmentation import (
    adjust_image)

path = 'test1.jpg'
image = colour.io.read_image(path)


#result = extract_colour_checkers_segmentation(image)
#result = detect_colour_checkers_segmentation(image, 16 , True)


checker_coordinates = colour_checkers_coordinates_segmentation(image)
print(checker_coordinates)


result = detect_colour_checkers_segmentation(image, 16 , True)

#print(result[0])


#print(result[0].swatch_colours)
#print(result[0].colour_checker_image)
#print(result[0].swatch_masks)

#plot_image(image)



SWATCHES = []

for swatches, colour_checker, masks in detect_colour_checkers_segmentation(
    image, additional_data=True):
    SWATCHES.append(swatches)

    # Using the additional data to plot the colour checker and masks.
    masks_i = np.zeros(colour_checker.shape)
    for i, mask in enumerate(masks):
        masks_i[mask[0]:mask[1], mask[2]:mask[3], ...] = 1
    plot_image(
        colour.cctf_encoding(np.clip(colour_checker - masks_i*1, 0, 1)));

"""
D65 = colour.ILLUMINANTS['CIE 1931 2 Degree Standard Observer']['D65']
REFERENCE_COLOUR_CHECKER = colour.COLOURCHECKERS['ColorChecker 2005']

REFERENCE_SWATCHES = colour.XYZ_to_RGB(
        colour.xyY_to_XYZ(list(REFERENCE_COLOUR_CHECKER.data.values())),
        REFERENCE_COLOUR_CHECKER.illuminant, D65,
        colour.RGB_COLOURSPACES['sRGB'].XYZ_to_RGB_matrix)

for i, swatches in enumerate(SWATCHES):
    swatches_xyY = colour.XYZ_to_xyY(colour.RGB_to_XYZ(
        swatches, D65, D65, colour.RGB_COLOURSPACES['sRGB'].RGB_to_XYZ_matrix))

    colour_checker = colour.characterisation.ColourChecker(
        os.path.basename(COLOUR_CHECKER_IMAGE_PATHS[i]),
        OrderedDict(zip(REFERENCE_COLOUR_CHECKER.data.keys(), swatches_xyY)),
        D65)
    
    plot_multi_colour_checkers(
        [REFERENCE_COLOUR_CHECKER, colour_checker])
    
    swatches_f = colour.colour_correction(swatches, swatches, REFERENCE_SWATCHES)
    swatches_f_xyY = colour.XYZ_to_xyY(colour.RGB_to_XYZ(
        swatches_f, D65, D65, colour.RGB_COLOURSPACES['sRGB'].RGB_to_XYZ_matrix))
    colour_checker = colour.characterisation.ColourChecker(
        '{0} - CC'.format(os.path.basename(COLOUR_CHECKER_IMAGE_PATHS[i])),
        OrderedDict(zip(REFERENCE_COLOUR_CHECKER.data.keys(), swatches_f_xyY)),
        D65)
    
 """