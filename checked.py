import os
import cv2
from PIL import Image
from colour import read_image
from colour_checker_detection import extract_colour_checkers_segmentation
from colour_checker_detection import detect_colour_checkers_segmentation

import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import numpy as np

import colour
from colour.plotting import *


def correct_image(path, actual_color, reference_colors):
    org_img = cv2.imread(path, cv2.COLOR_BGR2RGB)
    actual_color = np.array(actual_color)
    reference = np.array(reference_colors)
    #for i in range(0, ):
    #    for i in range(0, im.shape[0], n):
    #        im[i:i+n] = TPS_color_correction(im[i:i+n], extracted_color, reference_colors)
    w = org_img.shape[1];
    h = org_img.shape[0];

    NPs = actual_color.shape[0]
    NPg = reference.shape[0]
    Xp = actual_color[:, 0].conj().transpose()
    Yp = actual_color[:, 1].conj().transpose()
    Zp = actual_color[:, 2].conj().transpose()
    Xg = reference[:, 0].conj().transpose()
    Yg = reference[:, 1].conj().transpose()
    Zg = reference[:, 2].conj().transpose()

    rXp = np.tile(Xp.reshape(NPs, 1), (1, NPs))
    rYp = np.tile(Yp.reshape(NPs, 1), (1, NPs))
    rZp = np.tile(Zp.reshape(NPs, 1), (1, NPs))
    wR=np.sqrt(((rXp - rXp.conj().transpose()) ** 2 + (rYp - rYp.conj().transpose()) ** 2 + (rZp - rZp.conj().transpose()) ** 2))
    wK = (2 * (wR ** 2)) * np.log(wR + 1e-20)
    wP = np.hstack((np.ones((NPs, 1)), Xp.reshape(NPs, 1), Yp.reshape(NPs, 1), Zp.reshape(NPs, 1)))
    wL = np.vstack((np.hstack((wK, wP)),np.hstack((wP.conj().transpose(), np.zeros((4,4))))))
    wY = np.vstack((np.hstack((Xg.reshape(NPg, 1), Yg.reshape(NPg, 1), Zg.reshape(NPg, 1))),np.zeros((4, 3))))
    inv_wl=np.linalg.inv(wL)
    wW = np.dot(inv_wl, wY)





    data = np.zeros((h, w, 3), dtype=np.uint8)
    for i in range(0, h-1):
        for j in range(0, w-1):
            input_color = np.array([org_img[i][j]])
            X = input_color[:, 0].conj().transpose()
            Y = input_color[:, 1].conj().transpose()
            Z = input_color[:, 2].conj().transpose()
            NPss=input_color.shape[0]
            NWs = np.size(X, axis=0)
            rX = np.tile(X, (NPss,1))
            rY = np.tile(Y, (NPss,1))
            rZ = np.tile(Z, (NPss,1))
            rXp = np.tile(Xp.reshape(NPs, 1), (1,NWs))
            rYp = np.tile(Yp.reshape(NPs, 1), (1,NWs))
            rZp = np.tile(Zp.reshape(NPs, 1), (1,NWs))

            wR = np.sqrt((rXp - rX)**2 + (rYp - rY)**2 + (rZp - rZ)** 2)
            wK = (2 * (wR ** 2)) * np.log(wR + 1e-20)
            wP = np.hstack((np.ones((NWs, 1)), X.reshape(NPss,1), Y.reshape(NPss,1), Z.reshape(NPss,1))).conj().transpose()
            wL = np.vstack((wK,wP)).conj().transpose()

            Xw = np.dot(wL,wW[:, 0])
            Yw = np.dot(wL,wW[:, 1])
            Zw = np.dot(wL,wW[:, 2])
            data[i][j] = np.hstack((Xw.reshape(NPss,1), Yw.reshape(NPss,1), Zw.reshape(NPss,1)))
            
            #data[i][j]= [100,0,0]

    img = Image.fromarray(data, 'RGB')
    img.save('my.png')
    img.show()
    plot_image(org_img, img);        

def extract_color_checker_colors(file_path):
    image = read_image(path)
    result = detect_colour_checkers_segmentation(image, 16 , True)
    return [
        result[0].swatch_colours * 255,
        result[1].swatch_colours * 255
    ]

def TPS_color_correction(input_color,actual_color,reference):
    NPs = actual_color.shape[0]
    NPg = reference.shape[0]
    Xp = actual_color[:, 0].conj().transpose()
    Yp = actual_color[:, 1].conj().transpose()
    Zp = actual_color[:, 2].conj().transpose()
    Xg = reference[:, 0].conj().transpose()
    Yg = reference[:, 1].conj().transpose()
    Zg = reference[:, 2].conj().transpose()

    rXp = np.tile(Xp.reshape(NPs, 1), (1, NPs))
    rYp = np.tile(Yp.reshape(NPs, 1), (1, NPs))
    rZp = np.tile(Zp.reshape(NPs, 1), (1, NPs))
    wR=np.sqrt(((rXp - rXp.conj().transpose()) ** 2 + (rYp - rYp.conj().transpose()) ** 2 + (rZp - rZp.conj().transpose()) ** 2))
    wK = (2 * (wR ** 2)) * np.log(wR + 1e-20)
    wP = np.hstack((np.ones((NPs, 1)), Xp.reshape(NPs, 1), Yp.reshape(NPs, 1), Zp.reshape(NPs, 1)))
    wL = np.vstack((np.hstack((wK, wP)),np.hstack((wP.conj().transpose(), np.zeros((4,4))))))
    wY = np.vstack((np.hstack((Xg.reshape(NPg, 1), Yg.reshape(NPg, 1), Zg.reshape(NPg, 1))),np.zeros((4, 3))))
    inv_wl=np.linalg.inv(wL)
    wW = np.dot(inv_wl, wY)

    X = input_color[:, 0].conj().transpose()
    Y = input_color[:, 1].conj().transpose()
    Z = input_color[:, 2].conj().transpose()
    NPss=input_color.shape[0]
    NWs = np.size(X, axis=0)
    rX = np.tile(X, (NPss,1))
    rY = np.tile(Y, (NPss,1))
    rZ = np.tile(Z, (NPss,1))
    rXp = np.tile(Xp.reshape(NPs, 1), (1,NWs))
    rYp = np.tile(Yp.reshape(NPs, 1), (1,NWs))
    rZp = np.tile(Zp.reshape(NPs, 1), (1,NWs))

    wR = np.sqrt((rXp - rX)**2 + (rYp - rY)**2 + (rZp - rZ)** 2)
    wK = (2 * (wR ** 2)) * np.log(wR + 1e-20)
    wP = np.hstack((np.ones((NWs, 1)), X.reshape(NPss,1), Y.reshape(NPss,1), Z.reshape(NPss,1))).conj().transpose()
    wL = np.vstack((wK,wP)).conj().transpose()

    Xw = np.dot(wL,wW[:, 0])
    Yw = np.dot(wL,wW[:, 1])
    Zw = np.dot(wL,wW[:, 2])
    return np.hstack((Xw.reshape(NPss,1), Yw.reshape(NPss,1), Zw.reshape(NPss,1)))

reference_colors = [[115,82,68],
                    [194,150,130],
                    [98,122,157],
                    [87,108,67],
                    [133,128,177],
                    [103,189,170],
                    [214,126,44],
                    [80,91,166],
                    [193,90,99],
                    [94,60,108],
                    [157,188,64],
                    [224,163,46],
                    [56,61,150],
                    [70,148,73],
                    [175,54,60],
                    [231,199,31],
                    [187,86,149],
                    [8,133,161],
                    [243,243,242],
                    [200,200,200],
                    [160,160,160],
                    [121,122,121],
                    [85,85,85],
                    [52,52,52]]
extracted_color = [[121,95,67],[209,178,148],[139,146,171],[108,130,65],[171,156,182],[182,208,183],[215,149,59],[96,88,178],[201,109,99],[82,51,101],[190,209,92],[219,181,65],[54,46,151],[123,177,90],[188,83,53],[226,211,74],[200,108,151],[105,152,184],[235,231,219],[219,214,197],[196,189,172],[153,147,131],[75,84,80],[32,33,37]]

path = 'test.jpg'   
image = read_image(path)

result = detect_colour_checkers_segmentation(image, 16 , True)

#display section with color checker from the original image
#plot_image(colour.cctf_encoding(result[0].colour_checker_image))

#display those 24 colors from color checker in float format
#print(result[0].swatch_colours)



#swatches = result[0].swatch_colours
#regenerate the color checker colors with our format
#c = np.empty([4, 6, 3])
#for i in range(0,4):
#    for j in range(0,6):
#        c[i][j] = swatches[i*6+j]
#display generated color checker
#plot_image(colour.cctf_encoding(c, 'ProPhoto RGB'))


#test estimation
#estimated_color = TPS_color_correction(np.array([[150,150,150]]), np.array(extracted_color), np.array(reference_colors));
#print(estimated_color)

correct_image('test1.jpg', extracted_color, reference_colors)
